Expire User Password

Expire User Password is a module that allows you to manage password expiration 
of registered users on the site.

This module permits you to choose an expiration date for a password at various 
levels: global, positions and/or user level.

The module has the ability to notify users that their password has expired 
inviting them to update it for security reasons.
